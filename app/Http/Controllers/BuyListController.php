<?php

namespace App\Http\Controllers;

use App\BuyList;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BuyListController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buylists = Auth::user()->buylists;

        return view('buylists.home', compact('buylists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('buylists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate(['item' => 'required|between:2,50']);

        Auth::user()->buylists()->save(new BuyList($data));

        return redirect()->route('buylists.index')->withStatus('Buy list saved!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BuyList  $buyLists
     * @return \Illuminate\Http\Response
     */
    public function updateData(Request $request)
    {
        return response('asd');
        $data = $request->validate(['done' => 'required|boolean']);

        // $buyLists->done = $data['done'];
        // $buyLists->completed_on = $data['done'] == true ? Carbon::now() : null;

        // return response(['status' => $buyLists->save() ? 'success' : 'error']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BuyList  $buyLists
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $buyList = BuyList::find($id);
        $data = $request->validate(['done' => 'required|boolean']);

        $buyList->done = $data['done'];
        $buyList->completed_on = $data['done'] == true ? Carbon::now() : null;

        return response(['status' => $buyList->save() ? 'success' : 'error']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BuyList  $buyLists
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buyList = BuyList::find($id);
        return response(['status' => $buyList->delete() ? 'success' : 'error']);
    }
}
